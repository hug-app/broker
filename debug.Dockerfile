## Create Builder Container
FROM golang:1.22.2-alpine3.18 as builder
ENV CGO_ENABLED 0

# Make sure this path mirrors what you have in $GOPATH
ADD . /hug/broker/

# Install requirements for the app
RUN apk add --no-cache git

WORKDIR /hug/broker/

# Compile the app WITHOUT optimization flags, allows Delve to
# provide a better debug experience. This creates an executable `brokerApp`
# and looks under `go-remote-debug-tutorial/example-app` for the Go files.
RUN go build -gcflags "all=-N -l" -o /brokerApp ./cmd/api

# Allow delve to run on Alpine based containers.
RUN apk add --no-cache libc6-compat

# Install Delve
RUN go install github.com/go-delve/delve/cmd/dlv@latest
RUN go install github.com/cosmtrek/air@latest

RUN apk add --no-cache libc6-compat

# Expose debug port and application port
EXPOSE 40000 8080