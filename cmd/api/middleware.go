package main

import (
	"context"
	"net/http"
	"time"

	"github.com/google/uuid"
	"github.com/spf13/viper"
)

type contextKey string

const requestIDKey = contextKey("requestID")

func addTimeoutContext(next http.Handler) http.Handler {
	timeout := viper.GetInt("API_TIMEOUT")

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx, cancel := context.WithTimeout(r.Context(), time.Second*time.Duration(timeout))
		defer cancel()

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func addRequestID(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		requestID := uuid.New().String()

		ctx := context.WithValue(r.Context(), requestIDKey, requestID)

		w.Header().Set("X-Request-ID", requestID)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
