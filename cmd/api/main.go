package main

import (
	"fmt"
	"log"
	"math"
	"net/http"
	"strings"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/spf13/viper"

	"broker/event"
	"broker/zap"
)

const (
	webPort = "80"
)

type App struct {
	EventService *event.Service
	logger       *zap.Log
}

func main() {
	var (
		rabbitConn *amqp.Connection
		err        error
	)

	initViper()

	if rabbitConn, err = connect(); err != nil {
		log.Fatalf("Error establishing AMQP connection: %v", err)
	}

	defer func() {
		if err = rabbitConn.Close(); err != nil {
			log.Fatalf("Error closing AMQP connection: %v", err)
		}
	}()

	eventService := event.NewService(rabbitConn)

	app := App{
		EventService: &eventService,
	}

	l, err := zap.NewLog()
	if err != nil {
		log.Fatalf("Error initializing logger: %v", err)
	}

	app.logger = l

	log.Printf("Starting server at port %s\n", webPort)

	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", webPort),
		Handler: app.routes(),
	}

	if err = srv.ListenAndServe(); err != nil {
		log.Fatalf("Error starting the HTTP server: %v", err)
	}
}

func connect() (*amqp.Connection, error) {
	var counts int64
	var backOff = 1 * time.Second
	var connection *amqp.Connection

	for {
		c, err := amqp.Dial("amqp://guest:guest@rabbitmq")
		if err != nil {
			fmt.Println("RabbitMQ not yet ready")
			counts++
		} else {
			fmt.Println("Connected to RabbitMQ")
			connection = c
			break
		}

		if counts > 5 {
			fmt.Println(err)
			return nil, err
		}

		backOff = time.Duration(math.Pow(float64(counts), 2)) * time.Second
		time.Sleep(backOff)
		continue
	}

	return connection, nil
}

func initViper() {
	viper.SetConfigFile(".env")
	viper.SetConfigType("env")

	// Add the current directory to the paths to look for the config file
	viper.AddConfigPath(".")

	// Automatically replace `.` with `_` in environment variable keys
	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)

	// Read the config file
	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file: %v", err)
	}

	// Bind environment variables
	viper.AutomaticEnv()

	// Read the SERVICES variable

}
