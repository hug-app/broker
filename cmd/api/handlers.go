package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	amqp "github.com/rabbitmq/amqp091-go"
)

type RequestBody struct {
	Handler string          `json:"handler"`
	Payload json.RawMessage `json:"payload"`
}

type Response struct {
	Message string `json:"message"`
	Status  int    `json:"status"`
}

func (app *App) Broker(w http.ResponseWriter, r *http.Request) {
	payload := jsonResponse{
		Error:   false,
		Message: "Hit the broker!",
	}

	_ = app.writeJSON(w, http.StatusOK, payload)
}

func (app *App) HandleSubmission(w http.ResponseWriter, r *http.Request) {
	var (
		requestBody RequestBody
		replyQueue  *amqp.Queue
		err         error
		ctx         = r.Context()
	)

	correlationId := ctx.Value(requestIDKey).(string)

	err = app.readJSON(w, r, &requestBody)
	app.failOnError(ctx, requestBody.Handler, err, "Error reading requestBody", w)

	ch, err := app.EventService.OpenChannel()
	app.failOnError(ctx, requestBody.Handler, err, "Error opening channel", w)
	defer func(ch *amqp.Channel) {
		err = ch.Close()
		app.failOnError(ctx, requestBody.Handler, err, "Error closing channel", w)
	}(ch)

	replyQueue, err = app.EventService.ReplyQueueDeclare(ch, correlationId)
	app.failOnError(ctx, requestBody.Handler, err, "Error creating reply queue", w)

	err = app.pushToQueue(requestBody, correlationId, replyQueue.Name, ch)
	app.failOnError(ctx, requestBody.Handler, err, "Error pushing a task to a queue", w)

	a := ch.IsClosed()
	print(a)

	messages, err := app.EventService.GetMessagesFromReturnQueue(replyQueue.Name, ch)
	app.failOnError(ctx, requestBody.Handler, err, "Error getting messages from a reply queue", w)

	for d := range messages {
		if d.CorrelationId == correlationId {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			if err := json.NewEncoder(w).Encode(d.Body); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			return
		}
	}
}

func writeResponse(status int, message string, w http.ResponseWriter) {
	response := Response{
		Message: message,
		Status:  status,
	}

	// Set the content type to application/json
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)

	// Encode the response to JSON
	if err := json.NewEncoder(w).Encode(response); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (app *App) pushToQueue(request RequestBody, correlationID, replyQueueName string, ch *amqp.Channel) error {
	j, err := json.Marshal(&request)
	if err != nil {
		return err
	}

	//TODO empty queue name handling
	//queueName := getQueueName(request.Handler)

	err = app.EventService.Push(request.Handler, string(j), correlationID, replyQueueName, ch)
	a := ch.IsClosed()
	print(a)
	if err != nil {
		return err
	}

	return nil
}

func (app *App) failOnError(ctx context.Context, action string, err error, msg string, w http.ResponseWriter) {
	if err != nil {
		extra := map[string]string{
			"request_id": fmt.Sprintf("%s", ctx.Value(requestIDKey)),
		}

		app.logger.Error(ctx, action, msg+": "+err.Error(), extra)

		writeResponse(http.StatusInternalServerError, "Internal server error", w)

		log.Fatalf("%s: %s", msg, err)
	}
}
