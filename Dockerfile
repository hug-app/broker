# TODO return for server buildds
#FROM golang:1.22.2-alpine3.18 as builder
#
#RUN mkdir /app
#
#COPY . /app
#
#WORKDIR /app
#
#RUN CGO_ENABLED=0 go build -o brokerApp ./cmd/api
#
#RUN chmod +x /app/brokerApp

FROM alpine:latest

RUN mkdir /app

# TODO return for server buildds
#COPY --from=builder /app/brokerApp /app
COPY brokerApp /app

CMD [ "/app/brokerApp" ]