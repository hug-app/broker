BROKER_BINARY=brokerApp
LISTENER_BINARY=listenerApp
LOGGER_BINARY=loggerApp
USER_BINARY=userApp

up:
	@echo "Starting Docker images"
	docker-compose up -d
	@echo "Docker images started"

down:
	@echo "Stopping Docker containers"
	docker-compose down
	@echo "Docker containers stopped"

up_debug_broker:
	@echo "Starting Docker images"
	docker-compose -f ./docker-compose.debug.yaml up -d
	@echo "Docker images started"


up_build: build_broker build_listener build_logger build_user
	@echo "Stopping Docker containers"
	docker-compose down
#	@echo "Removing old image"
#	@images=$$(docker images 'broker-broker-service' -a -q); \
#        if [ -n "$$images" ]; then \
#			docker rmi $$images; \
#		fi
	@echo "Building Docker images and starting Docker containers"
	#docker-compose up --build -d
	@echo "Docker images are built and containers are started"

build_broker:
	@echo "Building Broker binary"
	env GOOS=linux CGO_ENABLED=0 go build -o ${BROKER_BINARY} ./cmd/api
	@echo "Done!"

build_listener:
	@echo "Building listener binary"
	cd ../listener && env GOOS=linux CGO_ENABLED=0 go build -o ${LISTENER_BINARY} .
	@echo "Done!"

build_logger:
	@echo "Building logger binary..."
	cd ../logger && env GOOS=linux CGO_ENABLED=0 go build -o ${LOGGER_BINARY} ./cmd/api
	@echo "Done!"

build_user:
	@echo "Building user binary..."
	cd ../user && env GOOS=linux CGO_ENABLED=0 go build -o ${USER_BINARY} ./cmd/api
	@echo "Done!"

docker_restart_debug:
	docker-compose down && docker-compose -f ./docker-compose.debug.yaml up -d

all:
	@echo "Let's go"