package event

import (
	"context"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
)

type Service struct {
	Connection *amqp.Connection
	Channel    *amqp.Channel
}

func (e *Service) OpenChannel() (*amqp.Channel, error) {
	channel, err := e.Connection.Channel()
	if err != nil {
		return nil, err
	}

	e.Channel = channel
	return channel, nil
}

func (e *Service) CloseChannel() {
	e.Channel.Close()
}

func (e *Service) Push(handler, msg, correlationID, replyQueueName string, channel *amqp.Channel) error {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	err := channel.PublishWithContext(
		ctx,
		"service_tasks",
		handler,
		false,
		false,
		amqp.Publishing{
			ReplyTo:       replyQueueName,
			CorrelationId: correlationID,
			ContentType:   "application/json",
			Body:          []byte(msg),
		},
	)
	a := channel.IsClosed()
	print(a)

	if err != nil {
		return err
	}

	return nil
}

func (e *Service) ReplyQueueDeclare(ch *amqp.Channel, requestId string) (*amqp.Queue, error) {
	q, err := ch.QueueDeclare(
		requestId, // name
		false,     // durable
		false,     // delete when unused
		true,      // exclusive
		false,     // no-wait
		nil,       // arguments
	)

	if err != nil {
		return nil, err
	}

	return &q, nil
}

func (e *Service) GetMessagesFromReturnQueue(q string, ch *amqp.Channel) (<-chan amqp.Delivery, error) {
	messages, err := ch.Consume(
		q,
		"",
		true,
		false,
		false,
		false,
		nil,
	)

	if err != nil {
		return nil, err
	}
	return messages, nil
}

func NewService(conn *amqp.Connection) Service {
	eventService := Service{
		Connection: conn,
	}

	return eventService
}
